// ensure that we can serve our app from a file path
module.exports = {
  publicPath: './',
  devServer: {
    watchOptions: {
      poll: true
    }
  }
}
