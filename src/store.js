import Vue from 'vue'
import Vuex from 'vuex'
import Products from '@/json/products.json'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: Products
  },
  mutations: {},
  actions: {},
  getters: {
    getProductById: state => id => {
      return state.products.find(product => product.productId === id)
    },
    getProductSetByIds: state => ids => {
      return state.products.filter(
        product => ids.indexOf(product.productId) !== -1
      )
    },
    getProducts: state => {
      return state.products
    }
  }
})
